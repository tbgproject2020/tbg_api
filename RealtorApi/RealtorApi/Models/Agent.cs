﻿using System;

namespace RealtorApi.Models
{
    public class Agent
    {
        public long agentid { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string email { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string socialsecurity { get; set; }
        public string homephone { get; set; }
        public string mobileno { get; set; }
        public string relicense { get; set; }
        public string boardname { get; set; }
        public string password { get; set; }
        public Nullable<System.DateTime> createdat { get; set; }
        public string createdby { get; set; }
        public Nullable<System.DateTime> modifiedat { get; set; }
        public string modifiedby { get; set; }
        public string isactive { get; set; }
    }
}
