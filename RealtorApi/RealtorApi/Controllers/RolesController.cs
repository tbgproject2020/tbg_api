﻿using BAL.Repositories;
using Model.ViewModels;
using System.Web.Http;

namespace RealtorApi.Controllers
{
    [RoutePrefix("api/Roles")]
    public class RolesController : ApiController
    {
        private readonly IRoleRepository _roleRepository;

        public RolesController()
        {
            _roleRepository = new RoleRepository();
        }

        [Route("SaveRole")]
        [HttpPost]
        public IHttpActionResult AddRole(RoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                return Ok(_roleRepository.AddRole(model));
            }
            return Ok(new BaseViewModel { Message = Result.Fail, Success = true });
        }

        public IHttpActionResult GetRoleList(int page, int pageSize, string search)
        {
            return Ok(_roleRepository.GetRoles(page, pageSize, search));
        }
    }
}