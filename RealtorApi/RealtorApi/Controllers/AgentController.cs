﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using BAL.Repositories;
using Model.ViewModels;

namespace RealtorApi.Controllers
{
    [RoutePrefix("api/Agent")]
    public class AgentController : ApiController
    {
        private readonly IAgentRepository _agentRepository;

        public AgentController()
        {
            _agentRepository = new AgentRepository();
        }

        //// GET: Agent
        [HttpGet]
        [Route("AddNewAgent")]
        public IHttpActionResult AddNewAgent(AgentViewModel model)
        {
            if (ModelState.IsValid)
            {
                return Ok(_agentRepository.CreateAgent(model));
            }
            return Ok(new BaseViewModel { Message = "Something went wrong" });
        }

        [Route("RegisterAgent")]
        [HttpGet]
        public IHttpActionResult RegisterAgent(string Email, string UserName, string Password)
        {
            return Ok(_agentRepository.RegisterAgent(Email, UserName, Password));
        }

        [Route("AgentPayment")]
        [HttpPost]
        public IHttpActionResult AgentPayment(AgentPayment model)
        {
            return Ok(_agentRepository.AentPayment(model));
        }

        [Route("AgentStatus")]
        [HttpPost]
        public IHttpActionResult AgentStatus(string status, string agentId)
        {
            return Ok(_agentRepository.AgentStatus(status, new Guid(agentId)));
        }

        [Route("GetAgents")]
        [HttpPost]
        public IHttpActionResult GetAgents()
        {
            return Ok(_agentRepository.GetAllAgent());
        }
    }
}