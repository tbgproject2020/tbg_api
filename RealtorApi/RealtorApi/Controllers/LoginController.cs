﻿using BAL.Repositories;
using Model.ViewModels;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RealtorApi.Controllers
{
    [RoutePrefix("api/Login")]
    public class LoginController : ApiController
    {
        private readonly LoginRepository _obj;

        public LoginController()
        {
            _obj = new LoginRepository();
        }

        [Route("IsAuthenticateLogin")]
        [HttpGet]
        public IHttpActionResult IsAuthenticateLogin(string Email, string Password)
        {
            return Ok(_obj.UserLogin(new LoginViewModel { Password = Password, UserId = Email }));
        }

        [Route("RegisterUser")]
        [HttpPost]
        public IHttpActionResult RegisterNewUser(LoginModel model)
        {
            if (model == null)
            {
                model = new LoginModel
                {
                    UserName = System.Web.HttpContext.Current.Request.Params["model.userName"],
                    UserEmail = System.Web.HttpContext.Current.Request.Params["model.userEmail"],
                    Password = System.Web.HttpContext.Current.Request.Params["model.password"]
                };
            }
            return Ok(_obj.UserRegister(model));
        }

        [Route("VerifyAccount")]
        [HttpGet]
        public HttpResponseMessage VerifyAccountEmail(string key)
        {
            //Here i need to redirect to a aspx page.
            var result = _obj.EmailVerification(key);

            var response = Request.CreateResponse(HttpStatusCode.Moved);
            response.Headers.Location = new Uri("http://localhost:4200/broker-login");

            return response;
        }

        [Route("ForgetPassword")]
        [HttpPost]
        public IHttpActionResult ForgetAgentPassword(string LoginId)
        {
            return Ok(_obj.CreateForgetKey(new System.Guid(LoginId)));
        }

        [Route("ResetPassword")]
        [HttpPost]
        public IHttpActionResult ResetAgentPassword(string LoginId)
        {
            return Ok(_obj.ForgetAgentPassword(System.Web.HttpContext.Current.Request.Params["key"].ToString()));
        }
    }
}