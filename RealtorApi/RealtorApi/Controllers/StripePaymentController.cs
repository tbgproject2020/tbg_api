﻿using Newtonsoft.Json;
using Stripe;
using System.Web.Mvc;

namespace RealtorApi.Controllers
{
    public class StripePaymentController : Controller
    {
        // GET: StripePayment
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public void charge(dynamic Model)
        {
            // Set your secret key. Remember to switch to your live secret key in production!
            // See your keys here: https://dashboard.stripe.com/account/apikeys
            StripeConfiguration.ApiKey = "sk_test_51GzAeCK65n36PaAZjfR4Ox7yMqKmxI27qoyEB4Lko8v7LQxWT6iavMC8IIAr3fIktw5o3yMzHSMs2AmZfaTVx7I900JgWSVoBr";

            // Token is created using Checkout or Elements!
            // Get the payment token submitted by the form:
            var token = Model.Token; // Using ASP.NET MVC

            var options = new ChargeCreateOptions
            {
                Amount = 999,
                Currency = "inr",
                Description = "Example charge",
                Source = token,
            };

            var service = new ChargeService();
            var charge = service.Create(options);

        }

    }

    [Route("create-payment-intent")]
    public class PaymentIntentApiController : Controller
    {
        [HttpPost]
        public ActionResult Create(PaymentIntentCreateRequest request)
        {
            var paymentIntents = new PaymentIntentService();
            var paymentIntent = paymentIntents.Create(new PaymentIntentCreateOptions
            {
                Amount = CalculateOrderAmount(request.Items),
                Currency = "inr",
            });

            return Json(new { clientSecret = paymentIntent.ClientSecret });
        }

        private int CalculateOrderAmount(Item[] items)
        {
            // Replace this constant with a calculation of the order's amount
            // Calculate the order total on the server to prevent
            // people from directly manipulating the amount on the client
            return 1400;
        }

        public class Item
        {
            [JsonProperty("id")]
            public string Id { get; set; }
        }

        public class PaymentIntentCreateRequest
        {
            [JsonProperty("items")]
            public Item[] Items { get; set; }
        }
    }
}

