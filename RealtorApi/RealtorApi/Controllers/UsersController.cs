﻿using BAL.Repositories;
using Model.ViewModels;
using System.Web.Http;

namespace RealtorApi.Controllers
{
    [RoutePrefix("api/Roles")]
    public class UsersController : ApiController
    {
        // GET: Users
        private readonly IUserRepository _roleRepository;

        public UsersController()
        {
            _roleRepository = new UserRepository();
        }

        [Route("SaveRole")]
        [HttpPost]
        public IHttpActionResult AddUser(AdminUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                return Ok(_roleRepository.AddUser(model));
            }
            return Ok(new BaseViewModel { Message = Result.Fail, Success = true });
        }

        public IHttpActionResult GetUserList(int page, int pageSize, string search)
        {
            return Ok(_roleRepository.GetUsers(page, pageSize, search));
        }
    }
}