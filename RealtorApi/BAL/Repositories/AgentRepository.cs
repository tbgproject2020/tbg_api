﻿using DAL.EntityModel;
using Model.ViewModels;
using PaymentGateway;
using Stripe;
using System;
using System.Linq;
using System.Net;
using System.Net.Mail;
//using System.Web.Mvc;

namespace BAL.Repositories
{
    public interface IAgentRepository
    {
        AgentModel GetAllAgent();
        BaseViewModel CreateAgent(AgentViewModel model);
        BaseViewModel DeleteAgent(Guid AgentId);
        BaseViewModel AcceptAgentLogin(Guid AgentId);
        BaseViewModel EditAgent(CommonModel model);
        BaseViewModel AgentLogin(LoginViewModel login);
        BaseViewModel AgentStatus(string status, Guid AgentId);
        BaseViewModel RegisterAgent(string Email, string UserName, string Password);

        BaseViewModel AentPayment(AgentPayment model);
    }

    public class AgentRepository : IAgentRepository
    {
        private readonly RealtorAppEntities _db;

        public AgentRepository()
        {
            _db = new RealtorAppEntities();
        }

        public BaseViewModel AgentStatus(string status, Guid AgentId)
        {
            try
            {
                var Agent = _db.AgentInfoes.Where(x => x.AgentId == AgentId).FirstOrDefault();
                Agent.Status = status;
                _db.SaveChanges();
            }
            catch (Exception)
            {
                return new BaseViewModel { CurrentUser = null, Message = Result.Fail, Success = false };
            }
            return new BaseViewModel { CurrentUser = null, Message = Result.Success, Success = true };
        }

        public BaseViewModel RegisterAgent(string Email, string UserName, string Password)
        {
            if (!_db.Logins.Any(x => x.Email.ToLower() == Email.ToLower()))
            {
                AgentLoginRequest Req = new AgentLoginRequest
                {
                    ReqLoginId = Guid.NewGuid(),
                    UserName = UserName,
                    Password = Password,
                    Email = Email,
                    CreatedAt = DateTime.Now,
                    CreatedBy = "sys",
                    IsVerified = false,
                    IsActive = false,
                    EmailVerificationKey = CommonModel.GetUniqueKey()
                };
                _db.AgentLoginRequests.Add(Req);
                // Send mail for Activation

                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                mail.From = new MailAddress("brahminrealtor@gmail.com");
                mail.To.Add(Req.Email);
                mail.Subject = "Email Verification";

                mail.Body = "Hi, Welcome to The Bramhin Group " + Req.UserName +
                    " . \n Please verify email. \n" + "http://localhost:50360" + "/api/Login/VerifyAccount?key=" + Req.EmailVerificationKey;

                mail.IsBodyHtml = true;
                SmtpServer.Port = 587;
                SmtpServer.Credentials = new NetworkCredential("brahminrealtor@gmail.com", "realtor@123");
                SmtpServer.EnableSsl = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                SmtpServer.Send(mail);

                _db.SaveChanges();

                return new BaseViewModel { CurrentUser = null, Message = Result.Success, Success = true };
            }
            else
            {
                return new BaseViewModel { CurrentUser = null, Message = Result.AlreadyExists, Success = true };
            }
        }

        public BaseViewModel CreateAgent(AgentViewModel model)
        {
            try
            {
                AgentInfo newAgent = new AgentInfo
                {
                    Address = model.Address,
                    AgentBio = model.AgentBio,
                    AgentId = Guid.NewGuid(),
                    AgentName = model.AgentName,
                    BoardName = model.BoardName,
                    BrokerNumber = model.BrokerNumber,
                    Brokers = model.Brokers,
                    CellPhone = model.CellPhone,
                    CompanyAddress = model.CompanyAddress,
                    CompanyName = model.CompanyName,
                    ContactName = model.ContactName,
                    ContactNumber = model.ContactNumber,
                    CreatedBy = "Admin",
                    CreatedDate = DateTime.Now,
                    Email = model.Email,
                    DriverLicense = model.DriverLicense,
                    EndDate = model.EndDate,
                    HomePhone = model.HomePhone,
                    HomesOrSoldUnit = model.HomesOrSoldUnit,
                    HowDoUKnow = model.HowDoUKnow,
                    IsActivate = model.IsActivate,
                    LeaveReasonOrComment = model.LeaveReasonOrComment,
                    Status = "Pending",
                    Relationship = model.Relationship,
                    ReLicense = model.ReLicense,
                    SalesValue = model.SalesValue,
                    SocialSecurity = model.SocialSecurity,
                    StartDate = model.StartDate,
                };
                _db.AgentInfoes.Add(newAgent);
                _db.SaveChanges();

            }
            catch (Exception)
            {
                return new BaseViewModel { CurrentUser = null, Message = Result.Fail, Success = false };
            }
            return new BaseViewModel { CurrentUser = null, Message = Result.Success, Success = true };
        }

        public BaseViewModel DeleteAgent(long id)
        {
            return new BaseViewModel { CurrentUser = null, Message = Result.Success, Success = true };
        }

        public BaseViewModel EditAgent(CommonModel model)
        {

            return new BaseViewModel { CurrentUser = null, Message = Result.Success, Success = true };
        }

        public AgentModel GetAllAgent()
        {
            try
            {
                var Agents = _db.AgentInfoes.Select(x => new AgentViewModel
                {
                    Address = x.Address,
                    AgentBio = x.AgentBio,
                    AgentId = x.AgentId,
                    AgentName = x.AgentName,
                    BioDocPath = x.BioDocPath,
                    BoardName = x.BoardName,
                    BrokerNumber = x.BrokerNumber,
                    CellPhone = x.CellPhone,
                    CompanyAddress = x.CompanyAddress,
                    CompanyName = x.CompanyName,
                    ContactName = x.ContactName,
                    ContactNumber = x.ContactNumber,
                    CreatedBy = x.CreatedBy,
                    CreatedDate = x.CreatedDate,
                    DocumentGenerated = x.DocumentGenerated,
                    DocumentPath = x.DocumentPath,
                    DriverLicense = x.DriverLicense,
                    Email = x.Email,
                    EndDate = x.EndDate,
                    HomePhone = x.HomePhone,
                    HomesOrSoldUnit = x.HomesOrSoldUnit,
                    HowDoUKnow = x.HowDoUKnow,
                    IsActivate = x.IsActivate,
                    LeaveReasonOrComment = x.LeaveReasonOrComment,
                    Relationship = x.Relationship,
                    ReLicense = x.ReLicense,
                    RemindDate = x.RemindDate,
                    SalesValue = x.SalesValue,
                    SocialSecurity = x.SocialSecurity,
                    StartDate = x.StartDate,
                    Signed = x.Signed
                }).ToList();

                AgentModel data = new AgentModel
                {
                    AgentList = Agents,
                    // StatusList = new List<SelectListItem> {
                    //new SelectListItem{ Text="Disable", Value = "Disable" },
                    //new SelectListItem{ Text="Approve", Value = "Approve" },
                    //new SelectListItem{ Text="Reject", Value = "Reject" }},
                    Message = Result.Success,
                    Success = true
                };
                return data;
            }
            catch (Exception)
            {
                return new AgentModel { Message = Result.Fail, Success = false };
            }

        }

        public CommonModel GetAgentById(long id)
        {
            throw new NotImplementedException();
        }

        public BaseViewModel AgentLogin(LoginViewModel login)
        {
            return new BaseViewModel { CurrentUser = null, Message = Result.Success, Success = true };
        }

        public BaseViewModel DeleteAgent(Guid AgentId)
        {
            return new BaseViewModel { CurrentUser = null, Message = Result.Success, Success = true };
        }

        public BaseViewModel AcceptAgentLogin(Guid AgentId)
        {
            try
            {
                var Agent = _db.AgentInfoes.Where(x => x.AgentId == AgentId).FirstOrDefault();
                Agent.IsActivate = true;
                var AgentLogin = _db.Logins.Where(x => x.UserId == Agent.AgentId).FirstOrDefault();
                AgentLogin.IsActive = true;
                _db.SaveChanges();
            }
            catch (Exception)
            {
                return new BaseViewModel { CurrentUser = null, Message = Result.Fail, Success = false };
            }
            return new BaseViewModel { CurrentUser = null, Message = Result.Success, Success = true };
        }

        public BaseViewModel AentPayment(AgentPayment model)
        {
            //var AgentLogin = _db.Logins.Where(x => x.LoginId == model.LoginId).FirstOrDefault();

            //AgentLogin.PayStatus = true;
            //_db.SaveChanges();

            //ActiveUser CUser = new ActiveUser
            //{
            //    PayDone = true,
            //    RoleName = "Agent",
            //    Signed = false,
            //    UserId = AgentLogin.LoginId,
            //    UserName = AgentLogin.UserName
            //};

            //AddressOptions Address = new AddressOptions
            //{
            //    City = model.Address.City,
            //    Country = model.Address.Country,
            //    Line1 = model.Address.Line1,
            //    Line2 = model.Address.Line2,
            //    PostalCode = model.Address.PostalCode,
            //    State = model.Address.State
            //};

            //CardDetais cardDetail = new CardDetais
            //{
            //    CardNumber = model.CardNumber,
            //    CVC = model.CVV,
            //    ExpMonth = model.ExpMonth,
            //    ExpYear = model.ExpYear
            //};

            // Create Account in stripe and make 0 Rs Initial Charge
            //dynamic customerAcc = Payment.VerifyCard(AgentLogin.UserName, AgentLogin.Email, Address, cardDetail);
            //Payment.CreateCustomer();
            return new BaseViewModel { CurrentUser = null, Message = Result.Success, Success = true , IsPayed = true, IsSigned = false};
        }
    }
}
