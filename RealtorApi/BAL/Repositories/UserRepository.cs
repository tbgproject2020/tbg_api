﻿using DAL.EntityModel;
using Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BAL.Repositories
{
    public interface IUserRepository
    {
        BaseViewModel AddUser(AdminUserViewModel model);
        List<AdminUserViewModel> GetUsers(int page, int pageSize, string search);
    }
    public class UserRepository : IUserRepository
    {
        private readonly RealtorAppEntities _db;

        public UserRepository()
        {
            _db = new RealtorAppEntities();
        }

        public BaseViewModel AddUser(AdminUserViewModel model)
        {
            try
            {
                if (model.UserId == Guid.Empty)
                {
                    User newUser = new User
                    {
                        CreatedAt = DateTime.Now,
                        IsActive = true,
                        CreatedBy = "System",
                        UserName = model.UserName,
                        RoleId = Guid.NewGuid()
                    };
                    _db.Users.Add(newUser);
                }
                else
                {
                    User user = _db.Users.Where(x => x.UserId == model.UserId).FirstOrDefault();
                    user.UserName = model.UserName;
                    user.RoleId = model.RoleId;
                    user.IsActive = model.IsActive;
                }

                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                return new BaseViewModel { CurrentUser = null, Message = Result.Fail, Success = false };
            }
            return new BaseViewModel { CurrentUser = null, Message = Result.Success, Success = true };
        }

        public List<AdminUserViewModel> GetUsers(int page, int pageSize, string search)
        {
            try
            {
                List<AdminUserViewModel> roles = _db.Users.Where
                    (x => x.UserName.StartsWith(search) || x.UserName.StartsWith(search) || x.UserName.Contains(search) || x.UserName.Contains(search)
                ).Select(x => new AdminUserViewModel
                {
                    UserName = x.UserName,
                    CreatedAt = x.CreatedAt,
                    IsActive = x.IsActive,
                    RoleId = x.RoleId != null ? x.RoleId : Guid.Empty,
                    CreatedBy = x.CreatedBy
                }).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }
            catch (Exception ex)
            {
                return new List<AdminUserViewModel>();
            }
            return new List<AdminUserViewModel>();
        }
    }
}
