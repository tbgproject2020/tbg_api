﻿using DAL.EntityModel;
using Model.ViewModels;
using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;

namespace BAL.Repositories
{
    public interface ILoginRepository
    {
        BaseViewModel UserLogin(LoginViewModel model);
        BaseViewModel UserRegister(LoginModel model);
        BaseViewModel EmailVerification(string Key);
        BaseViewModel ForgetAgentPassword(string Key);
    }

    public class LoginRepository : ILoginRepository
    {
        private readonly RealtorAppEntities _db;

        public LoginRepository()
        {
            _db = new RealtorAppEntities();
        }

        public BaseViewModel EmailVerification(string Key)
        {
            try
            {
                var LoginReq = _db.AgentLoginRequests.Where(x => x.EmailVerificationKey == Key).FirstOrDefault();
                if (LoginReq != null)
                {
                    LoginReq.IsVerified = true;

                    Login login = new Login
                    {
                        CreatedAt = DateTime.Now,
                        KeyVerified = false,
                        CreatedBy = "",
                        Email = LoginReq.Email,
                        Password = LoginReq.Password,
                        IsActive = false,
                        LoginId = Guid.NewGuid(),
                        UserName = LoginReq.UserName,
                        RoleId = _db.Roles.Where(x => x.RoleName.ToLower() == "agent").FirstOrDefault().RoleId
                    };

                    _db.Logins.Add(login);

                    string stripeCustomerId = string.Empty;
                    if (!string.IsNullOrEmpty(LoginReq.UserName) && !string.IsNullOrEmpty(LoginReq.Email))
                    {
                        stripeCustomerId = PaymentGateway.Payment.CreateCustomer(LoginReq.UserName, LoginReq.Email);
                    }

                    if (!string.IsNullOrEmpty(stripeCustomerId))
                    {
                        StripeCusAccount stripeCusAccount = new StripeCusAccount
                        {
                            LoginId = login.LoginId,
                            //CustomerId = stripeCustomerId, TO DO
                            clientSecretKey = stripeCustomerId,
                            IsActive = true
                        };
                        var stripeAcc = _db.StripeCusAccounts.Add(stripeCusAccount);
                    }
                    _db.SaveChanges();
                }
                else
                {
                    return new BaseViewModel { CurrentUser = null, Message = "Account already verified.", Success = true };
                }
                return new BaseViewModel { CurrentUser = null, Message = Result.Success, Success = true };
            }
            catch (Exception)
            {
                // Add Log
                return new BaseViewModel { CurrentUser = null, Message = Result.Fail, Success = false };
            }
        }

        public BaseViewModel CreateForgetKey(Guid LoginId)
        {
            try
            {
                if (LoginId != Guid.Empty)
                {
                    var Login = _db.Logins.Where(m => m.LoginId == LoginId).FirstOrDefault();
                    Login.LoginVerificationKey = CommonModel.GetUniqueKey();
                    Login.KeyVerified = false;
                    _db.SaveChanges();

                    // Send mail for Activation
                    MailMessage mail = new MailMessage();
                    SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                    mail.From = new MailAddress("aarti@coodeit.com");
                    mail.To.Add(Login.Email);
                    mail.Subject = "Email Verification";
                    mail.Body = "Hi, Welcome to The Bramhin Group " + Login.UserName +
                        " . /n Please click on button to confirm your account." +
                         "api heading" + "/Login/VerifyAccount?key=" + Login.LoginVerificationKey;

                    mail.IsBodyHtml = true;
                    SmtpServer.Port = 587;
                    SmtpServer.Credentials = new NetworkCredential("aarti@coodeit.com", "arti@@123");
                    SmtpServer.EnableSsl = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    SmtpServer.Send(mail);

                    return new BaseViewModel { CurrentUser = null, Message = Result.Success, Success = true };
                }
            }
            catch (Exception)
            {
                return new BaseViewModel { CurrentUser = null, Message = Result.Fail, Success = false };
            };

            return new BaseViewModel { CurrentUser = null, Message = Result.Fail, Success = true };
        }

        public BaseViewModel ForgetAgentPassword(string Key)
        {
            try
            {
                var Login = _db.Logins.Where(x => x.LoginVerificationKey == Key).FirstOrDefault();
                if (Login == null)
                {
                    return new BaseViewModel { CurrentUser = null, Message = Result.DataNotFound, Success = true };
                }
                return new BaseViewModel { CurrentUser = null, Message = Result.Success, Success = true };
            }
            catch (Exception)
            {
                // Add Log
                return new BaseViewModel { CurrentUser = null, Message = Result.Fail, Success = false };
            }
        }

        public BaseViewModel UserLogin(LoginViewModel model)
        {
            try
            {
                var Login = _db.Logins.Where(x => x.Email.ToLower() == model.UserId.ToLower())
                    .Select(m => new ActiveUser
                    {
                        UserId = m.LoginId != null ? m.LoginId : Guid.Empty,
                        UserName = m.UserName,
                        RoleName = m.User.Role.RoleName
                    }).FirstOrDefault();
                if (Login != null)
                {
                    return new BaseViewModel { CurrentUser = Login, Message = Result.Success, Success = true };
                }
            }
            catch (System.Exception)
            {
                // Add Log
                return new BaseViewModel { CurrentUser = null, Message = Result.Fail, Success = false };
            }
            return new BaseViewModel { CurrentUser = null, Message = Result.DataNotFound, Success = true };
        }

        public BaseViewModel UserRegister(LoginModel model)
        {
            try
            {
                var Logins = _db.Logins.ToList();
                if (!Logins.Any(x => x.Email.ToLower() == model.UserEmail.ToLower()))
                {
                    string VerificationKey = CommonModel.GetUniqueKey();

                    Login newLogin = new Login
                    {
                        UserName = model.UserName,
                        Email = model.UserEmail,
                        Password = model.Password,
                        LoginId = Guid.NewGuid(),
                        IsActive = false,
                        RoleId = _db.Roles.Where(m => m.RoleName.ToLower() == "agent").FirstOrDefault().RoleId,
                        KeyVerified = false,
                        LoginVerificationKey = VerificationKey,
                        CreatedAt = DateTime.Now,
                        CreatedBy = "System"
                    };

                    var result = _db.Logins.Add(newLogin);

                    System.Net.Configuration.SmtpSection secObj = (System.Net.Configuration.SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");

                    using (MailMessage mm = new MailMessage())
                    {
                        mm.From = new MailAddress(secObj.From); //--- Email address of the sender
                        mm.To.Add(newLogin.Email); //---- Email address of the recipient.
                        mm.Subject = "We are trying to send email using SMTP settings."; //---- Subject of email.
                        mm.Body = "Hello this is content of the email \n Please click on following url to continew." + ""; //---- Content of email.

                        SmtpClient smtp = new SmtpClient
                        {
                            Host = secObj.Network.Host, //---- SMTP Host Details. 
                            EnableSsl = secObj.Network.EnableSsl //---- Specify whether host accepts SSL Connections or not.
                        };
                        NetworkCredential NetworkCred = new NetworkCredential(secObj.Network.UserName, secObj.Network.Password);
                        //---Your Email and password
                        smtp.UseDefaultCredentials = true;
                        smtp.Credentials = NetworkCred;
                        smtp.Port = 587; //---- SMTP Server port number. This varies from host to host. 
                        smtp.Send(mm);
                    }

                    //MailMessage mail = new MailMessage();
                    //SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                    //mail.From = new MailAddress("artiahir47@gmail.com");
                    //mail.To.Add("aarti@coodeit.com.com");
                    //mail.Subject = "filename";
                    //mail.Body = "Hi, Welcome to The Bramhin Group " + model.UserName +
                    //    " . /n Please click on button to confirm your account." +
                    //     "api heading" + "http://localhost:50360/Login/VerifyAccount?key=" + VerificationKey;

                    //SmtpServer.Port = 25;
                    //SmtpServer.Credentials = new System.Net.NetworkCredential("artiahir47@gmail.com", "alskdjfhg@1357");
                    //SmtpServer.EnableSsl = true;
                    //SmtpServer.Send(mail);

                    //MailMessage mail = new MailMessage();
                    //SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                    //mail.From = new MailAddress("aarti@coodeit.com");
                    //mail.To.Add("artiahir47@gmail.com");
                    //mail.Subject = "Test Mail";
                    //mail.Subject = "Email Verification";
                    //mail.Body = "Hi, Welcome to The Bramhin Group " + model.UserName +
                    //    " . /n Please click on button to confirm your account." +
                    //     "api heading" + "http://localhost:50360/Login/VerifyAccount?key=" + VerificationKey;

                    //SmtpServer.Port = 587;
                    //SmtpServer.Credentials = new System.Net.NetworkCredential("aarti@coodeit.com", "arti@@123");
                    //SmtpServer.EnableSsl = true;

                    //SmtpServer.Send(mail);

                    // Send mail for Activation
                    //MailMessage mail = new MailMessage();
                    //SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                    //mail.From = new MailAddress("artiahir47@gmail.com");
                    //mail.To.Add(model.UserEmail);


                    //SmtpServer.UseDefaultCredentials = true;
                    //mail.IsBodyHtml = true;
                    //SmtpServer.Port = 587;
                    //SmtpServer.Credentials = new NetworkCredential("artiahir47@gmail.com", "arti@@123");
                    //SmtpServer.EnableSsl = true;
                    ////ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    //SmtpServer.Send(mail);

                    _db.SaveChanges();



                    return new BaseViewModel { CurrentUser = null, Message = Result.Success, Success = true };
                }
                else
                {
                    return new BaseViewModel { CurrentUser = null, Message = Result.AlreadyExists, Success = true };
                }
            }
            catch (System.Exception)
            {
                // Add Log
                return new BaseViewModel { CurrentUser = null, Message = Result.Fail, Success = false };
            }
        }
    }
}
