﻿using DAL.EntityModel;
using Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BAL.Repositories
{
    public interface IRoleRepository
    {
        BaseViewModel AddRole(RoleViewModel model);
        List<AdminUserViewModel> GetRoles(int page, int pageSize, string search);
    }
    public class RoleRepository : IRoleRepository
    {
        private readonly RealtorAppEntities _db;

        public RoleRepository()
        {
            _db = new RealtorAppEntities();
        }

        public BaseViewModel AddRole(RoleViewModel model)
        {
            try
            {
                if (model.RoleId == Guid.Empty)
                {
                    Role newRole = new Role
                    {
                        CreatedAt = DateTime.Now,
                        IsActive = true,
                        CreatedBy = "System",
                        RoleName = model.RoleName,
                        RoleId = Guid.NewGuid()
                    };
                    _db.Roles.Add(newRole);
                }
                else
                {
                    Role role = _db.Roles.Where(x => x.RoleId == model.RoleId).FirstOrDefault();
                    role.RoleName = model.RoleName;
                    role.RoleId = model.RoleId;
                    role.IsActive = model.IsActive;
                }

                _db.SaveChanges();
            }
            catch (Exception)
            {
                return new BaseViewModel { CurrentUser = null, Message = Result.Fail, Success = false };
            }
            return new BaseViewModel { CurrentUser = null, Message = Result.Success, Success = true };
        }

        public List<AdminUserViewModel> GetRoles(int page, int pageSize, string search)
        {
            try
            {
                List<RoleViewModel> roles = _db.Roles.Where
                    (x => x.RoleName.StartsWith(search) || x.RoleName.StartsWith(search) || x.RoleName.Contains(search) || x.RoleName.Contains(search)
                ).Select(x => new RoleViewModel
                {
                    RoleName = x.RoleName,
                    CreatedAt = x.CreatedAt,
                    IsActive = x.IsActive,
                    RoleId = x.RoleId != null ? x.RoleId : Guid.Empty,
                    CreatedBy = x.CreatedBy
                }).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }
            catch (Exception)
            {
                return new List<AdminUserViewModel>();
            }
            return new List<AdminUserViewModel>();
        }
    }
}
