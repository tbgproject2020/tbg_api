﻿using Model.ViewModels;
using Stripe;

namespace PaymentGateway
{
    public class Payment
    {
        public Payment()
        {
            StripeConfiguration.ApiKey = Urls.ApiSKey;
        }

        // Step 1 Create Customer
        public static string CreateCustomer(string username, string email)
        {
            try
            {
                var requestOptions = new RequestOptions
                {
                    ApiKey = Urls.ApiSecretKey
                };

                var options = new CustomerCreateOptions
                {
                    Name = username,
                    Email = email
                };

                var customerService = new CustomerService();
                var customer = customerService.Create(options, requestOptions);

                return customer.Id;
            }
            catch (System.Exception ex)
            {
                //Log DB Error to trace Exception
                return string.Empty;
            }
        }

        //public static void AcceptCardPayment()
        //{
        //    var paymentIntents = new PaymentIntentService();

        //    var paymentIntent = paymentIntents.Create(new PaymentIntentCreateOptions
        //    {

        //        Amount = 0,
        //        Currency = "usd",
        //        PaymentMethod = "Card",
        //        ReceiptEmail = "aarti@coodeit.com",
        //        ReturnUrl = "",

        //    }, new RequestOptions
        //    {
        //        ApiKey = StripeConfiguration.ApiKey
        //    });

        //    //return Json(new {
        //    //    clientSecret = paymentIntent.ClientSecret,
        //    //    paymentIntent.ApplicationId,
        //    //    paymentIntent.
        //    //});
        //}

        //public static void SavingCardDetails()
        //{

        //    // Create a Customer:
        //    var customerOptions = new CustomerCreateOptions
        //    {

        //        //Source = "tok_mastercard",
        //        Email = "paying.user@example.com",
        //    };
        //    var customerService = new CustomerService();
        //    Customer customer = customerService.Create(customerOptions);

        //    // Charge the Customer instead of the card:
        //    var chargeOptions = new ChargeCreateOptions
        //    {
        //        Amount = 1000,
        //        Currency = "usd",
        //        Customer = customer.Id,
        //    };
        //    var chargeService = new ChargeService();
        //    Charge charge = chargeService.Create(chargeOptions);

        //    // YOUR CODE: Save the customer ID and other info in a database for later.

        //}

        //public static dynamic CreateStripeAccount()
        //{

        //    var reqOption = new RequestOptions
        //    {
        //        ApiKey = Urls.ApiPKey
        //    };

        //    //    // Create Account for stripe
        //    var options = new AccountCreateOptions
        //    {
        //        Type = "custom",
        //        Country = "IN",
        //        Email = "aarti@coodeit.com",
        //        Capabilities = new AccountCapabilitiesOptions
        //        {
        //            CardPayments = new AccountCapabilitiesCardPaymentsOptions
        //            {
        //                Requested = true,
        //            },
        //            Transfers = new AccountCapabilitiesTransfersOptions
        //            {
        //                Requested = true,
        //            },
        //        },

        //    };
        //    var CreateAccservice = new AccountService();
        //    var createdAcc = CreateAccservice.Create(options,reqOption);

        //    // Attach Card 

        //    var AddCard = new ExternalAccountCreateOptions
        //    {
        //        ExternalAccount = "tok_visa_debit",
        //        DefaultForCurrency = false,
        //    };

        //    var ExternalService = new ExternalAccountService();
        //    var account = ExternalService.Create(createdAcc.Id, AddCard);

        //    var x = createdAcc.Id;
        //    var y = account.Id;


        //    return ExternalService;
        //}

        //public static PaymentIntent VerifyCard(string Name, string Email, AddressOptions address, CardDetais card)
        //{
        //    // Set your secret key. Remember to switch to your live secret key in production!
        //    // See your keys here: https://dashboard.stripe.com/account/apikeys
        //    StripeConfiguration.ApiKey = Urls.ApiSecretKey;

        //    // Create Account for stripe
        //    var options = new AccountCreateOptions
        //    {
        //        Type = "custom",
        //        Country = "IN",
        //        Email = Email,
        //        Capabilities = new AccountCapabilitiesOptions
        //        {
        //            CardPayments = new AccountCapabilitiesCardPaymentsOptions
        //            {
        //                Requested = true,
        //            },
        //            Transfers = new AccountCapabilitiesTransfersOptions
        //            {
        //                Requested = true,
        //            },
        //        },
        //        ExternalAccount = 
        //    };
        //    var service = new AccountService();
        //    service.Create(options);


        //    PaymentIntent Account = new PaymentIntent
        //    {
        //        Customer = new Customer
        //        {
        //            Name = Name,
        //            Email = Email,
        //            Address = new Stripe.Address
        //            {
        //                City = address.City,
        //                Country = address.Country,
        //                Line1 = address.Line1,
        //                Line2 = address.Line2,
        //                PostalCode = address.PostalCode,
        //                State = address.State
        //            },
        //            Currency = "inr",
        //            Description = "Test Customer Verify"
        //        },
        //        Description = "Test Payment Intentent Verify",
        //        ReceiptEmail = Email,
        //        Currency = "inr",
        //        Charges =
        //        {
        //            Data = new List<Charge>
        //            {
        //                new Charge{ Currency = "inr",ReceiptEmail =Email,Customer = new Customer
        //                {
        //                   Email = Email,
        //                   Currency = "inr",
        //                   Address = {
        //                        City = address.City,
        //                        Country = address.Country,
        //                        Line1 =address.Line1,
        //                        Line2 = address.Line2,
        //                        PostalCode = address.PostalCode,
        //                        State = address.State
        //                    },
        //                   Name = "Initial Charge",

        //                },
        //                Amount = 0,
        //                }

        //            }
        //        }
        //    };

        //    //var options = new TokenCreateOptions
        //    //{
        //    //    Account = new TokenAccountOptions
        //    //    {
        //    //        Individual = new TokenAccountIndividualOptions
        //    //        {
        //    //            FirstName = "Jane",
        //    //            LastName = "Doe",
        //    //        },
        //    //        TosShownAndAccepted = true,
        //    //    },
        //    //};
        //    //var service = new TokenService();
        //    //service.Create(options);

        //    // Create a Customer:
        //    //var customerOptions = new CustomerCreateOptions
        //    //{
        //    //    //SourceToken = "tok_mastercard",
        //    //    Description = "",
        //    //    Name = Name,
        //    //    Email = Email,
        //    //    Address = new AddressOptions
        //    //    {
        //    //        City = Address.City,
        //    //        Country = Address.Country,
        //    //        Line1 = Address.Line1,
        //    //        Line2 = Address.Line2,
        //    //        PostalCode = Address.PostalCode,
        //    //        State = Address.State
        //    //    }
        //    //};
        //    //var customerService = new CustomerService();
        //    //Customer customer = customerService.Create(customerOptions);

        //    ////// When it's time to charge the customer again, retrieve the customer ID.
        //    //var Chargeoptions = new ChargeCreateOptions
        //    //{
        //    //    Amount = 0, // $15.00 this time
        //    //    Currency = "inr",
        //    //    Customer = customer.Id, // Previously stored, then retrieved
        //    //};
        //    //var chargeservice = new ChargeService();
        //    //Charge charge = chargeservice.Create(Chargeoptions);

        //    var reqOption = new RequestOptions
        //    {
        //        ApiKey = Urls.ApiSecretKey
        //    };

        //    var TokenOptions = new TokenCreateOptions
        //    {
        //        Card = new CreditCardOptions
        //        {
        //            Number = card.CardNumber,
        //            ExpYear = card.ExpYear,
        //            ExpMonth = card.ExpMonth,
        //            Cvc = card.CVC
        //        }
        //    };

        //    var service = new TokenService();
        //    Token stripeToken = service.Create(TokenOptions, reqOption);
        //    return new PaymentIntent();
        //    //return new StripAccountModel { CustomerId = stripeToken.Id, ChargeId = stripeToken.ClientIp };
        //}


        //public static PaymentIntent PaymentIntentCreate()
        //{
        //    // Set your secret key. Remember to switch to your live secret key in production!
        //    // See your keys here: https://dashboard.stripe.com/account/apikeys
        //    //"sk_test_51GzAeCK65n36PaAZjfR4Ox7yMqKmxI27qoyEB4Lko8v7LQxWT6iavMC8IIAr3fIktw5o3yMzHSMs2AmZfaTVx7I900JgWSVoBr";

        //    var options = new PaymentIntentCreateOptions
        //    {
        //        Amount = 10, // add selected plan value
        //        Currency = "inr",
        //        PaymentMethodTypes = new List<string> { "card" },
        //        ReceiptEmail = "aarti@coodeit.com"
        //    };

        //    var reqOptions = new RequestOptions
        //    {
        //        ApiKey = "sk_test_51HDTVFDBKpKc7hKmQ58oBudN6JTaJwL45HWWWuvvtlYQfeYxtNgIfuiOkhuOP351wPIBuVDBzvyl3J8KGn6LaZjp00tPg94etg",
        //        StripeAccount = ""
        //    };

        //    //var reqCharge = new 

        //    var xService = new ChargeService();
        //    Charge charge = xService.Get("ch_1HEtWIDBKpKc7hKmneUMf3MB", null, reqOptions);

        //    var service = new PaymentIntentService();

        //    return service.Create(options);
        //}

        //public static dynamic CreateCustomer(string Name, string Email)
        //{
        //    var options = new CustomerCreateOptions
        //    {
        //        Description = "Hi This is my test account",
        //        Name = Name,
        //        Address = new AddressOptions { City = "", Country = "", Line1 = "", Line2 = "", PostalCode = "", State = "" },
        //        Email = Email
        //    };
        //    var service = new CustomerService();
        //    var customerAc = service.Create(options);
        //    return customerAc.Id;
        //}

        //public static dynamic CreateCard(string CustomerId)source
        //{
        //    var options = new CardCreateOptions
        //    {
        //        Source = "tok_mastercard",

        //    };
        //    var service = new CardService();
        //    return service.Create(CustomerId, options);
        //}

        //public static dynamic AuthenticateUser(string Name, string Email)
        //{
        //    var options = new RequestOptions
        //    {
        //        ApiKey = Urls.ApiSecretKey
        //    };
        //    var service = new ChargeService();
        //    Charge charge = service.Get("ch_1HEt7d2eZvKYlo2CHRR57n03", null, options);
        //    charge.Currency = "inr";
        //    charge.Customer = CreateCustomer(Name, Email);

        //    return string.Empty;
        //}
    }
}
