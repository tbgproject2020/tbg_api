﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Model.ViewModels
{
    #region Common Strings
    public static class Result
    {
        public static string Success = "Success";
        public static string Fail = "Fail";
        public static string DataNotFound = "NotFound";
        public static string AlreadyExists = "Exists";
    }
    #endregion

    public static class CreateCaredResponse
    {
        public static string customerId { get; set; }
        public static string chargeId { get; set; }
    }

    public static class CardCreateOptionsModel
    {
        public static string AddressCity { get; set; }
        public static string AddressCountry { get; set; }
        public static string AddressLine1 { get; set; }
        public static string AddressLine2 { get; set; }
        public static string AddressState { get; set; }
        public static string AddressZip { get; set; }
        public static string Cvc { get; set; }
        
        public static string Number { get; set; }
        public static string Name { get; set; }
        public static long ExpYear { get; set; }
        public static long ExpMonth { get; set; }
    }

    public static class Urls
    {
        public static string PaymentUrl = "https://api.stripe.com";
        public static string ApiPKey = "pk_test_51HDTVFDBKpKc7hKmwGoOs2w4AU88u6gctCXvfqti4VH8CinidrPX2pPMXp5EiGZKO4zGTVlcsRdNtjdqiE8kTEpC00QCphUqlE";
        public static string ApiSKey = "sk_test_51HDTVFDBKpKc7hKmQ58oBudN6JTaJwL45HWWWuvvtlYQfeYxtNgIfuiOkhuOP351wPIBuVDBzvyl3J8KGn6LaZjp00tPg94etg";

        public static string ApiSecretKey = "sk_test_51GzAeCK65n36PaAZjfR4Ox7yMqKmxI27qoyEB4Lko8v7LQxWT6iavMC8IIAr3fIktw5o3yMzHSMs2AmZfaTVx7I900JgWSVoBr";
        public static string ApiPubliceKey = "pk_test_51GzAeCK65n36PaAZfpt9aB6CDIdXhq9LJM72eIqZdHDQvGSRTiqGf5BJfgRwuNJGnxT6FOcdivNLPDbs9Yd8yOu800RmIVEU6F";
        //public static string 
    }

    public class CardDetais
    {
        public string CardNumber { get; set; }
        public long ExpYear { get; set; }
        public long ExpMonth { get; set; }
        public string CVC { get; set; }
    }

    public class AddressOptionsView
    {
        public string City { get; set; }
        public string Country { get; set; }
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string PostalCode { get; set; }
        public string State { get; set; }
    }

    public class StripAccountModel
    {
        public string CustomerId { get; set; }
        public string ChargeId { get; set; }
    }

    public class ActiveUser
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string RoleName { get; set; }

        public bool PayDone { get; set; }
        public bool Signed { get; set; }
    }

    public class BaseViewModel
    {
        public ActiveUser CurrentUser { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }

        public bool IsPayed { get; set; }
        public bool IsSigned { get; set; }
    }

    public class LoginViewModel
    {
        public string UserId { get; set; }
        public string Password { get; set; }
    }

    public class RoleViewModel : BaseViewModel
    {
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }

    public class AdminUserViewModel
    {
        public System.Guid UserId { get; set; }
        public string UserName { get; set; }
        public Nullable<System.Guid> RoleId { get; set; }
        public Nullable<System.Guid> LoginId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public string CreatedBy { get; set; }
    }

    public class LoginModel
    {
        public System.Guid LoginId { get; set; }
        public Nullable<System.Guid> UserId { get; set; }

        // For Registration
        public string UserEmail { get; set; }

        public string UserName { get; set; }
        public string Password { get; set; }
        public Nullable<System.Guid> RoleId { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }

        //Role Data
        public RoleViewModel UserRole { get; set; }
    }

    public class AgentModel : BaseViewModel
    {
        public AgentViewModel AgentData { get; set; }
        public List<AgentViewModel> AgentList { get; set; }
        public List<SelectListItem> StatusList { get; set; }

    }

    public class AgentPayment
    {
        public Guid LoginId { get; set; }

        public bool IsRepeateMonthly { get; set; }

        public string CardNumber { get; set; }
        public long ExpMonth { get; set; }
        public long ExpYear { get; set; }
        public string CVV { get; set; }

        public string PlanType { get; set; }
        public AddressOptionsView Address { get; set; }
    }

    public class AgentViewModel : BaseViewModel
    {
        public System.Guid AgentId { get; set; }
        public string AgentName { get; set; }
        public string Email { get; set; }
        public string SocialSecurity { get; set; }
        public string ReLicense { get; set; }
        public string HomePhone { get; set; }
        public string CellPhone { get; set; }
        public string Address { get; set; }
        public string BoardName { get; set; }
        public string ContactName { get; set; }
        public string ContactNumber { get; set; }
        public string Relationship { get; set; }
        public string CompanyName { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string CompanyAddress { get; set; }
        public string Brokers { get; set; }
        public string BrokerNumber { get; set; }
        public string SalesValue { get; set; }
        public string HomesOrSoldUnit { get; set; }
        public string LeaveReasonOrComment { get; set; }
        public string HowDoUKnow { get; set; }
        public Nullable<System.Guid> LoginId { get; set; }
        public Nullable<bool> DriverLicense { get; set; }
        public string LicenseDocPath { get; set; }
        public Nullable<bool> AgentBio { get; set; }
        public string BioDocPath { get; set; }
        public Nullable<bool> DocumentGenerated { get; set; }
        public Nullable<bool> Signed { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string DocumentPath { get; set; }
        public Nullable<bool> IsActivate { get; set; }
        public Nullable<bool> Reminder { get; set; }
        public Nullable<System.DateTime> RemindDate { get; set; }

        // All 3 doc data

    }

    public class TBGContract
    {
        public System.Guid TBGId { get; set; }
        public string Name { get; set; }
        public string Contractor { get; set; }
        public string DocumentPath { get; set; }
        public Nullable<bool> Signed { get; set; }
        public Nullable<bool> Reminder { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> RemindDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
    }

    public class W9
    {
        public System.Guid W9Id { get; set; }
        public string Name { get; set; }
        public string BusinessOrEntityName { get; set; }
        public Nullable<bool> LLC { get; set; }
        public Nullable<bool> CCorporation { get; set; }
        public Nullable<bool> SCorporation { get; set; }
        public Nullable<bool> Liability { get; set; }
        public Nullable<bool> Partnership { get; set; }
        public Nullable<bool> TrustOrEstate { get; set; }
        public string ClsTax { get; set; }
        public Nullable<bool> Other { get; set; }
        public string OtherInstructions { get; set; }
        public string Address { get; set; }
        public string CityStateZip { get; set; }
        public string AccountNo { get; set; }
        public string ReqNameAndAddress { get; set; }
        public string SocialSecurityNo { get; set; }
        public string EmIdNo { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string DocPath { get; set; }
        public Nullable<System.Guid> AgentId { get; set; }
        public Nullable<bool> Signed { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public Nullable<bool> Reminder { get; set; }
    }

    public class Authorization
    {
        public System.Guid AuthorizationId { get; set; }
        public Nullable<System.Guid> AgentId { get; set; }
        public Nullable<System.Guid> PlanId { get; set; }
        public Nullable<bool> ChargeMonthly { get; set; }
        public string CreditCardNo { get; set; }
        public string Expiration { get; set; }
        public Nullable<int> CVV { get; set; }
        public string BillingAddress { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }

    public class CommonModel
    {
        public CommonModel()
        {

        }

        public static string GetUniqueKey()
        {
            long ticks = DateTime.Now.Ticks;
            byte[] bytes = BitConverter.GetBytes(ticks);
            string Key = Convert.ToBase64String(bytes).Replace('+', '_').Replace('/', '-').TrimEnd('=');
            return Key;
        }

    }
}
